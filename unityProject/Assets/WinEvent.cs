﻿using UnityEngine;
using System.Collections;

public class WinEvent : MonoBehaviour {

	// Use this for initialization
	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.layer == LayerMask.NameToLayer("Player")) {
			Application.LoadLevel(Application.loadedLevel);

		}
	}
}
