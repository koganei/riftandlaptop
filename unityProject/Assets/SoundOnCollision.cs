﻿using UnityEngine;
using System.Collections;

public class SoundOnCollision : MonoBehaviour {

	public AudioClip groundHit;
	public AudioClip otherStoneHit;

	public AnimationCurve volumeCurve;

	private AudioSource audioSource;

	void Start() {
		audioSource = GetComponent<AudioSource> ();
	}

	AudioClip getRandomImpactSound() {
		AudioClip[] clips = new AudioClip[9];

		clips[0] = Resources.Load ("Sound/fx_stone_impact_01 -") as AudioClip;
		clips[1] = Resources.Load ("Sound/fx_stone_impact_02 -") as AudioClip;
		clips[2] = Resources.Load ("Sound/fx_stone_impact_03 -") as AudioClip;
		clips[3] = Resources.Load ("Sound/fx_stone_impact_04 -") as AudioClip;
		clips[4] = Resources.Load ("Sound/fx_stone_impact_05 -") as AudioClip;
		clips[5] = Resources.Load ("Sound/fx_stone_move_01 -") as AudioClip;
		clips[6] = Resources.Load ("Sound/fx_stone_move_02 -") as AudioClip;
		clips[7] = Resources.Load ("Sound/fx_stone_move_03 -") as AudioClip;
		clips[8] = Resources.Load ("Sound/fx_stone_move_04 -") as AudioClip;

		return clips [Random.Range(0, clips.Length)];

	}

	// Use this for initialization
	void OnCollisionEnter(Collision collision) {
		if (audioSource != null && collision.collider.gameObject.layer == LayerMask.NameToLayer ("Ground")) {
			float volumeToSet = volumeCurve.Evaluate(rigidbody.velocity.magnitude / 10.0f) * 10 + 0.2f;
			if(volumeToSet <= 0.2f) { return; }
			audioSource.volume = volumeToSet;
			audioSource.clip = groundHit;
			audioSource.Play();
		}

		if (audioSource != null && collision.collider.gameObject.layer == LayerMask.NameToLayer ("Block")) {
			float volumeToSet = volumeCurve.Evaluate(rigidbody.velocity.magnitude / 10.0f) * 10 + 0.2f;
			if(volumeToSet <= 0.2f) { return; }
			audioSource.volume = volumeToSet;
			audioSource.clip = getRandomImpactSound();
			audioSource.Play();
		}
		
	}
}
