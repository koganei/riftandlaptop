﻿using UnityEngine;
using System.Collections;

public class PulseLight : MonoBehaviour {

	public float timeBetweenPulses = 5.0f;
	public float lightIntensity = 6.0f;

	private float timer = 0.0f;
	private bool isPulsing = false;
	private Light lightComponent;

	private AudioSource audioSource;
	
	void Start() {
		lightComponent = GetComponent<Light> ();
		audioSource = GetComponent<AudioSource> ();
	}
		
	AudioClip getRandomThunderSound() {
		AudioClip[] clips = new AudioClip[3];
		
		clips[0] = Resources.Load ("Sound/thunder01") as AudioClip;
		clips[1] = Resources.Load ("Sound/thunder02") as AudioClip;
		clips[2] = Resources.Load ("Sound/thunder03") as AudioClip;
		return clips [Random.Range(0, clips.Length)];
		
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;

		if (timer > timeBetweenPulses && !isPulsing) {
			// we place the circle at a random distance from the parent
			transform.localPosition = new Vector3(Random.Range(0f, 50f), transform.localPosition.y, Random.Range(0f, 50f));
			isPulsing = true;
		}

		if (isPulsing) {
			pulse();
		}
	}

	void pulse() {

		if (lightComponent.intensity == lightIntensity) {
			timer = 0.0f;
			isPulsing = false;
			lightComponent.intensity = 0.0f;
		} else {

			if(lightComponent.intensity == 0.0f) {
				audioSource.clip = getRandomThunderSound();
				audioSource.Play();
			}

			lightComponent.intensity = Mathf.Lerp (lightComponent.intensity, lightIntensity, 0.5f);
		}
	}
}
