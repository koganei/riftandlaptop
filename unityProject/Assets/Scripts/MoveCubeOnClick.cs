﻿using UnityEngine;
using System.Collections;

public class MoveCubeOnClick : MonoBehaviour {

	void OnMouseDown () {
		transform.position = transform.position + transform.forward * 2;
	}
}