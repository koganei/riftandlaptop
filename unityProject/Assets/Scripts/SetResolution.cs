﻿using UnityEngine;
using System.Collections;

public class SetResolution : MonoBehaviour {

	public Texture2D cursorTexture;
	public CursorMode cursorMode = CursorMode.ForceSoftware;
	public Vector2 hotSpot = Vector2.zero;
	
	// Use this for initialization
	void Start () {
		Screen.SetResolution(2560, 800, false);
		OVRPlayerController controller = GameObject.Find ("OVRPlayerController").GetComponent<OVRPlayerController> ();
		// controller.SetAllowMouseRotation (false);
		Screen.showCursor = false;
		// Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
	}
	
	// Update is called once per frame
	void Update () {
		if (Screen.showCursor) {
			Screen.showCursor = false;
		}
	}

	void OnGUI () {
		// Make a background box
		GUI.Box(new Rect(10,10,350,25), "Swipe blocks quickly with your index to squash the runner.");

	}
}
