﻿using UnityEngine;
using System.Collections;

public class SwipeToSlide : MonoBehaviour {

	public float forceFactor = 1f;
	private bool currentlySwiping = false;
	private Vector3 originalPosition;
	private float startTime;


	// Use this for initialization
	void OnMouseDown() {
		currentlySwiping = true;
		originalPosition = Input.mousePosition;
		startTime = Time.time;
	}

	void OnMouseUp() {
		currentlySwiping = false;
		float endTime = Time.time;
		Vector3 endPosition = Input.mousePosition;

		Vector3 difference = endPosition - originalPosition;
		Vector3 force;

		// if there's no drag
		if (difference.magnitude == 0) {
			return;
		}

		if (Mathf.Abs(difference.x) > Mathf.Abs(difference.y)) {
			// we move in the x position
			force = new Vector3 (difference.magnitude * difference.x / Mathf.Abs(difference.x), 0, 0);
		} else {
			// we move in the z direction
			force = new Vector3(0,0,difference.magnitude * difference.y / Mathf.Abs(difference.y));
		}
			
		force /= (endTime - startTime);
		
		rigidbody.AddForce (force * forceFactor);

	}
}
