﻿using UnityEngine;
using System.Collections;

public class MoveWithMouse : MonoBehaviour {

	private Camera cam;

	// Use this for initialization
	void Start () {
		GameObject godCameraObj = GameObject.Find ("God/GodCamera");
		if (godCameraObj != null) {
			cam = godCameraObj.GetComponent<Camera> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(cam != null) {

			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			transform.position = ray.origin + (ray.direction * 340);
		}
	}
}
