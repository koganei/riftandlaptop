	using UnityEngine;
using System.Collections;

public class Map : MonoBehaviour {
	
	public GameObject BaseCube;

	public GameObject WinZone;
	
	public GameObject Player;

	public GameObject Tree1;
	public GameObject Tree2;

	public GameObject CreditCube;

	Vector2 TopLeft = new Vector2 (-100, -100);
	Vector2 BottomRight = new Vector2 (300,300);
	
	GameObject NewBlock (Vector3 position) {
		return Instantiate(BaseCube, position, Quaternion.Euler(0,Random.Range(0,359),0)) as GameObject;
	}
	
	GameObject NewTree(float which, Vector3 position) {

		GameObject tree;
		GameObject inst;
		Vector3 size;
		Quaternion rot;

		if (which > 0.2) {
			size = new Vector3 (Random.Range (100, 175), Random.Range (150, 200), Random.Range (100, 175));
			rot = Quaternion.Euler(-90,Random.Range(0,359),0);
			tree = Tree1;
		} else {
			size = new Vector3 (Random.Range (1, 3), Random.Range (1, 5), Random.Range (1, 3));
			rot = Quaternion.Euler(0,Random.Range(0,359),0);
			position.y = position.y - (size.y * 2f);
			tree = Tree2;
		}

		position.x = position.x + (size.x * Random.value);
		inst = Instantiate(tree, position, rot) as GameObject;
		inst.transform.localScale = size;

		return inst;
	}

	
	bool CreateNewBlock() {
		float odds = 0.5f;
		return (Random.value > odds);
	}

	bool CreateNewTree() {
		float odds = 0.9f;
		return (Random.value > odds);
	}


	//Place the Spawn POint Marker. 
	Vector2 SpawnPoint(Vector2 TopLeft, Vector2 BottomRight) {
		int x = Random.Range((int)TopLeft.x, (int)(BottomRight.x * 0.4));
		int y = Random.Range((int)TopLeft.y, (int)(BottomRight.y * 0.4));

		return new Vector2 (x, y);
	}

	Vector2 WinPoint(Vector2 TopLeft, Vector2 BottomRight) {

		int x = Random.Range((int)TopLeft.x + (int)(BottomRight.x * 0.7), (int)(BottomRight.x));
		int y = Random.Range((int)TopLeft.y + (int)(BottomRight.y * 0.7), (int)(BottomRight.y));

		return new Vector2 (x, y);
	}


	void Start () {
		int x = -100;

		int x_max = 250;
		int z_max = 250;

		Vector2 spawn_xy = SpawnPoint(TopLeft, BottomRight);
		Vector2 win_point = WinPoint(TopLeft, BottomRight);
		//Debug.Log (spawn_xy);
	//	Debug.Log (win_point);

		Instantiate (WinZone, new Vector3 (win_point.x, 0, win_point.y), Quaternion.identity);

		GameObject created_block;
		GameObject created_tree;


		Player.transform.position = new Vector3 (spawn_xy.x, Player.transform.position.y, spawn_xy.y);
		GameObject spawnTree = NewTree (1.0f, new Vector3 (spawn_xy.x, 0, spawn_xy.y));
		spawnTree.transform.position = new Vector3 (spawn_xy.x + 5f, 0, spawn_xy.y);

		Vector3 cc_pos = new Vector3 (Player.transform.position.x, Player.transform.position.y, Player.transform.position.z + -5);
		GameObject cc = Instantiate (CreditCube, cc_pos, Quaternion.identity) as GameObject;

		Vector3[] cubes = new Vector3[3];
		GameObject[] trees = new GameObject[2];


		cubes[0] =  new Vector3(10,10,10);
		cubes[1] =  new Vector3(10,20,5);
		cubes[2] =  new Vector3(5,25,5);
		
		while (x < x_max) {
			
			int z = -100;
			while(z < z_max) {

				if(CreateNewBlock()) {
					var pos = new Vector3(x * 2.0F, 1.0F, z * 2.0F);
					var size = cubes[Random.Range(0,cubes.Length)];
					pos.x = pos.x + (size.x * Random.value);
					pos.y = size.y * 0.5f;
					//pos.y = 0.0f;
					created_block = NewBlock(pos);
					created_block.transform.localScale = size;
					//created_block.transform.localScale = new Vector3();
				} 
				else if (CreateNewTree()) {
					var pos = new Vector3(x * 2.0F, 0.0F, z * 2.0F);
					NewTree(Random.value, pos);
				}	

				z = z + 7;
			}
			x = x + 7;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}