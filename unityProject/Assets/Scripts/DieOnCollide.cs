﻿using UnityEngine;
using System.Collections;

public class DieOnCollide : MonoBehaviour {

	// Use this for initialization
	void OnCollisionEnter(Collision collision) {
		if (collision.collider.gameObject.layer == LayerMask.NameToLayer ("Block")) { 
			if(collision.collider.rigidbody.velocity.magnitude > 0) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}
}
